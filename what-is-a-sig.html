---
title: What is a SIG?
---
<h1>{{ page.title }}</h1>

<p>A SIG is a “Special Interest Group”.  It is a collection of people who 
    are investigating a well-defined, specific topic.  
    That topic is both relevant to LEAPS WG3’s activity and of interest 
    across multiple LEAPS facilities.</p>

<p>SIGs are a mechanism through which LEAPS WG3 achieves (technical) work. 
    Over time, a SIG should have some set of tangible outcomes.  
    Those outcomes can be the production of documents; however, a SIG 
    should be effective by triggering and supporting some desired change 
    at multiple LEAPS facilities.</p>

<h2>How is a SIG formed?</h2>
<p>A SIG is created by LEAPS WG3 when a topic for work is proposed by one 
    WG3 member and people from three other facilities indicate that they 
    are interested in working on the proposed topic.</p>
<p>To facilitate this process, the person proposing a SIG might prepare 
    a (short) presentation, explaining the problem the SIG will address 
    and the work of the SIG to resolve that problem.  Such a presentation 
    may take place during a WG3 meeting.  Alternatively, a SIG proposal 
    may be discussed via the WG3 mailing list, or by some other method 
    of communication.</p>
<p>Once a SIG is agreed, LEAPS WG3 will create resources to support that 
    SIGs activities.  (Currently, this is simply creating a mailing list.)  
    The SIG proposer is tasked with initiating the SIG (for example, by 
    inviting people to join the mailing list and organising the first 
    meeting).  The newly founded SIG should quickly achieve the following 
    tasks:
    <ul>
        <li>Confirming the SIG’s title.</li>
        <li>Identifying the SIG’s coordinator, which may simply be the SIG 
            proposer.</li>
        <li>Writing the SIG’s mandate.</li>
    </ul></p>
<p>The SIG’s title, coordinator and mandate are published on the WG3 
    website as the initial description of the SIG.  This is to give the 
    SIG visibility, and so to garner more interest.</p>

<h2>Who can join a SIG?</h2>
<p>In general, there is no restriction on SIG membership.  We wish to 
    foster a sense of collaboration, and (as much as possible) SIGs 
    should be inclusive.</p>
<p>That said, a SIG may be tasked to investigate some sensitive topic, 
    one for which there needs to be careful control over the flow of 
    information.  For such a SIG, there will likely need a vetting 
    process to understand whether to allow someone to join.</p>
<p>Whatever the process, a SIG’s membership criteria should be clearly 
    and publicly stated in the SIG’s mandate.  The process of applying 
    for membership should be handled in a transparent and neutral 
    fashion.</p>

<h2>What is a SIG’s mandate?</h2>
<p>The mandate is a short document that describes what work the SIG will 
    undertake: what is the scope.  As far as it makes sense, the mandate 
    should also identify related activity that the SIG will NOT address; 
    i.e., what’s “off-topic”.</p>
<p>The mandate should describe who is eligible to join the SIG; 
    for example, is the SIG open to anyone, only people working at LEAPS 
    facilities, or limited to people matching some criteria.  The mandate 
    should also describe the mechanism through which someone can request 
    membership of the SIG, and the process through which that request is 
    processed.</p>
<p>The mandate is not set in stone, but may be modified as circumstances 
    change.  Any changes to the mandate should be agreed within the SIG 
    and announced appropriately.</p>
<p>The goal of a mandate is to support the SIG.  It is an advert for the 
    SIG, allowing external people to understand what is the group’s 
    activity and (potentially) attracting new members.   Ultimately, 
    it should help the SIG achieve its goals.</p>

<h2>How is a SIG structured?</h2>
<p>For each SIG, there is a member of LEAPS WG3 who is the coordinator.  
    It is recommended that each SIG also has a deputy coordinator, 
    someone who can take over should the coordinator be unavailable.</p>
<p>The coordinator is responsible for the overall running of the SIG.  
    The coordinator (or a delegated representative) will organise 
    meetings, structure the SIG (if it has any internal structure), 
    assign roles to members of the SIG (where appropriate), and manage 
    SIG-related communication.</p>
<p>The coordinator is responsible for managing the SIG’s membership, in 
    accordance with the SIG’s mandate.</p>

<h2>What kind of problem should a SIG solve?</h2>
<p>A SIG can address any issue or challenge that affects multiple LEAPS 
    facilities, and where the LEAPS WG3 mandate (IT) provides a 
    significant role.  While the various aspects of creating and handling 
    data provides a rich source of cross-facility activity, SIGs are not 
    limited to data-related topics.</p>

<h2>What responsibilities does a SIG have?</h2>
<p>Within the context of the SIG, the members of that SIG are expected 
    to work according to their mandate.</p>
<p>The SIG coordinator is expected to report to WG3 on the group’s 
    activity at least twice a year.  Such reports should include any 
    results (documents written, changes at LEAPS facilities, publications,
     etc), participation at events, etc.  These reports could be at WG3 
     meetings or via the WG3 mailing list.</p>
<p>The SIG coordinator is responsible for reporting back to LEAPS WG3 on 
    any topics that the members of the SIG feel would be appropriate to 
    raise within other LEAPS bodies; for example, within the Research and 
    Development Board (RDB) or the Coordination Board (CB).  The RDB 
    provides the confluence between all LEAPS WGs or SGs, allowing for 
    technical-focused discussion.  The CB provides the governance of LEAPS
     itself; it also  provides our connection to the directors of LEAPS 
     facilities. Members of LEAPS WG3 will help effect this communication 
     further within LEAPS.</p>
<p>The SIG coordinator is expected to liaise with the WG3 spokesperson 
    if either have identified concerns with how the SIG is operating, 
    and discuss how such concerns might be resolved.</p>

<h2>When does a SIG conclude?</h2>
<p>A SIG may be closed at any time. This could be the result of the 
    members of the SIG fulfilling the SIG’s mandate or if the members 
    simply decide the topic is not worth pursuing further.</p>
<p>The SIG coordinator should provide a final report written when a SIG 
    concludes.</p>
<p>This report should summarise all the achievements of the SIG; this is 
    to support garnering the fullest benefits from the time and effort 
    invested by the SIG’s members. The report may also include an 
    introspective “lessons learnt” section (to guide future, related 
    activities) and might also include a list of possible follow-up 
    activities.</p>
