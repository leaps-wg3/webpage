# check_search_api.rb

require 'httparty'
require 'yaml'
require 'json'
require 'date'

def check_search_api_endpoint(endpoint_url)
    queryIdentify_url = endpoint_url + "/Datasets/count"
    begin
        response = HTTParty.get(queryIdentify_url)
        if response.body.nil? || response.body.empty?
            puts "    Endpoint returned an empty body."
            return "Error", nil
        end

        response_json = JSON.parse(response.body)
        count = response_json["count"]
        if count.nil?
            puts "    Response missing dataset count."
            return "Error", nil
        end

        puts "    Endpoint is functional: reported #{count} datasets."
        return "Active", count
    rescue StandardError => e
        puts "    Error occurred while accessing endpoint: #{e.message}"
        return "Error", nil
    rescue JSON::JSONError => e
        puts "    Failed to parse JSON response: #{e.message}"
        return "Error", nil
    end
end

up_to_date = true
file_path = '_data/facilities.yml'
facilities = YAML.safe_load(File.read(file_path), permitted_classes: [Date])

facilities.each do |facility|
    odr = facility['odr']
    next unless odr && odr.key?('pan-search-api')
    search_api = odr['pan-search-api']

    next unless search_api.key?('link')
    endpoint = search_api['link']

    expected_status = search_api['status']
    expected_count = search_api['dataset_count']

    puts "Checking PaN Search API endpoint for #{facility['short-name']}: #{endpoint}"
    status, count = check_search_api_endpoint(endpoint)
    puts "    Result #{status} #{count}"

    search_api['last-check'] = Date.today #Time.now.strftime("%Y-%m-%d").to_date

    next if expected_status == status && expected_count == count

    search_api['status'] = status
    if count == nil
        search_api.delete('dataset_count')
    else
        search_api['dataset_count'] = count
    end

    up_to_date = false
end

File.open(file_path, 'w') {|f| f.write facilities.to_yaml } # unless up_to_date

exit(1) unless up_to_date
