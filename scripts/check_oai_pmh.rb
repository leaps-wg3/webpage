# check_oai_pmh.rb

require 'httparty'
require 'nokogiri'
require 'yaml'
require 'date'

def check_oai_pmh_endpoint(endpoint_url)
    queryIdentify_url = endpoint_url + "?verb=Identify"
    begin
        response = HTTParty.head(queryIdentify_url)
        if response.success?
            response = HTTParty.get(queryIdentify_url)
            if !(response.body.nil? || response.body.empty?)
                xml_response = Nokogiri::XML(response.body)
                oai_pmh_tag = xml_response.at_xpath('//*[name()="OAI-PMH"]')
                if oai_pmh_tag
                    puts "Endpoint is functional. OAI-PMH tag found in response."
                    return "Active"
                else
                    puts "Endpoint does not contain OAI-PMH tag."
                    return "Error"
                end
            else
                puts "Endpoint returned an empty body."
                return "Error"
            end
        else
            puts "Endpoint returned HTTP status code #{response.code}."
            return "Error"
        end
    rescue StandardError => e
        puts "Error occurred while accessing endpoint: #{e.message}"
        return "Error"
    end
end

up_to_date = true
facilities = YAML.safe_load(File.read('_data/facilities.yml'), permitted_classes: [Date])

facilities.each do |facility|
    odr = facility['odr']
    if odr && odr.key?('oai-pmh-endpoint')
        oai_pmh_endpoint = odr['oai-pmh-endpoint']['link']
        if oai_pmh_endpoint
            expected_status = odr['oai-pmh-endpoint']['status']
            puts "Checking OAI-PMH endpoint for #{facility['short-name']}: #{oai_pmh_endpoint}"
            endpoint_status = check_oai_pmh_endpoint(oai_pmh_endpoint)
            status_match = expected_status == endpoint_status
            puts "Error: Expected status '#{expected_status}' for #{facility['short-name']} OAI-PMH endpoint, but found status '#{endpoint_status}'." unless status_match
            up_to_date &= status_match
        end
    end
end

exit(1) unless up_to_date
