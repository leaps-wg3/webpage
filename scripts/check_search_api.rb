# check_search_api.rb

require 'httparty'
require 'yaml'
require 'json'
require 'date'

def check_search_api_endpoint(endpoint_url)
    queryIdentify_url = endpoint_url + "/Datasets/count"
    begin
        response = HTTParty.get(queryIdentify_url)
        if response.body.nil? || response.body.empty?
            puts "Endpoint returned an empty body."
            return "Error"
        end

        response_json = JSON.parse(response.body)
        count = response_json["count"]
        if count.nil?
            puts "Response missing dataset count."
            return "Error"
        end

        puts "Endpoint is functional: reported #{count} datasets."
        return "Active"
    rescue StandardError => e
        puts "Error occurred while accessing endpoint: #{e.message}"
        return "Error"
    rescue JSON::JSONError => e
        puts "Failed to parse JSON response: #{e.message}"
        return "Error"
    end
end

up_to_date = true
facilities = YAML.safe_load(File.read('_data/facilities.yml'), permitted_classes: [Date])

facilities.each do |facility|
    odr = facility['odr']
    if odr && odr.key?('pan-search-api')
        endpoint = odr['pan-search-api']['link']
        if endpoint
            expected_status = odr['pan-search-api']['status']
            puts "Checking PaN Search API endpoint for #{facility['short-name']}: #{endpoint}"
            endpoint_status = check_search_api_endpoint(endpoint)
            status_match = expected_status == endpoint_status
            puts "Error: Expected status '#{expected_status}' for #{facility['short-name']} PaN Search API endpoint, but found status '#{endpoint_status}'." unless status_match
            up_to_date &= status_match
        end
    end
end

exit(1) unless up_to_date
