# update_oai_pmh.rb

require 'httparty'
require 'yaml'
require 'date'
require 'nokogiri'
require 'persistent_httparty'

# OAI-PMH Metadata format namespaces for DataCite where the DataCite record
# is the metadata.
DATACITE_NAMESPACES = [ "http://datacite.org/schema/kernel-3",
    "http://datacite.org/schema/kernel-3/",
    "http://datacite.org/schema/kernel-4"]


# OAI-PMH Metadata format namespaces for DataCite where the DataCite record
# is wrapped and appears as the payload.
DATACITE_WRAPPED_NAMESPACES = [ "http://schema.datacite.org/oai/oai-1.1/" ]



class FailedOaiPmhRequest < StandardError
    def initialize(type, msg)
        super(type + ": " + msg)
    end
end

class OaiPmhClient
    include HTTParty

    persistent_connection_adapter

    headers 'User-Agent' => 'LEAPS-WG3-client/0.1'

    def initialize(oai_pmh_endpoint, from_id=nil)
        # WORK_AROUND: self.class.base_uri is normalised by adding a final
        # slash if it's missing.  This is wrong, because some endpoints
        # (legitimately) require there not to be a final slash.
        @base_uri = oai_pmh_endpoint
        self.class.headers "From" => from_id if from_id

        # WORK_AROUND: The connection adapter is persistent across multiple
        # URLs (which is good), but the adapter+connection-caching somehow
        # results in all requests being sent to the same host.
        #
        # To work-around this, we clear the connection caching code, forcing
        # a new connection caching.
        #
        # This is acceptable, as we process all requests from a single endpoint
        # before moving onto the next request and we process endpoints
        # sequentially.
        conn_adapter = self.class.default_options[:connection_adapter]
        conn_adapter.persistent_http = nil
    end

    def identify()
        response = self.class.get(@base_uri + "?verb=Identify")
        handle_response("Identify", response)
    end

    def list_sets()
        response = self.class.get(@base_uri + "?verb=ListSets")
        handle_response("ListSets", response)
    end

    def list_metadata_formats()
        response = self.class.get(@base_uri + "?verb=ListMetadataFormats")
        handle_response("ListMetadataFormats", response)
    end

    def list_identifiers(metadata_prefix: nil, resumption_token: nil)
        raise ArgumentError, 'Need to specify either metadata_prefix or resumption_token' unless metadata_prefix || resumption_token
        raise ArgumentError, 'Must specify one of metadata_prefix or resumption_token' if metadata_prefix && resumption_token

        if metadata_prefix
            arg="metadataPrefix="+metadata_prefix
        else
            arg="resumptionToken="+resumption_token
        end

        # Whether the server has already responded successfully in the past.
        # Using the presence of the resumptionToken to determine this is,
        # perhaps, questionable.
        hasServerResponded = resumption_token

        response = nil
        attempts = 0
        while !response
            begin
                attempts += 1

                starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
                response = self.class.get(@base_uri + "?verb=ListIdentifiers&" + arg)
                ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
                $stats.accept(ending - starting)

                if response.code >= 500
                    # 5xx status code indicates a server error.  Retrying might
                    # help.
                    raise HTTParty::Error.new "Received status code #{response.code}"
                elsif response.code >= 400
                    # 4xx status code indicates a client error.  Retrying with
                    # the same request will not help.
                    raise FailedOaiPmhRequest.new("ListIdentifiers", "Received status code #{response.code}")
                end
            rescue HTTParty::Error, SocketError, Timeout::Error => e
                # FIXME output needs to take into account current whether to
                # insert a new-line.  As a quick hack, use the presence of
                # resumptionToken in the URL to indicate a newline is needed
                # (this works most of the time, but not always)
                if hasServerResponded && attempts == 1
                    print "\n"
                end
                print_with_time("Attempt #{attempts} of #{MAX_HTTP_GET_RETRIES} failed: #{e.message}")

                if attempts >= MAX_HTTP_GET_RETRIES
                    # REVISIT include the URL in the message?
                    raise FailedOaiPmhRequest.new("ListIdentifiers", "Too many GET requests failed")
                end

                delay = FAILED_REQUEST_INITIAL
                if hasServerResponded
                    delay += attempts * FAILED_REQUEST_BACKOFF
                end

                sleep(delay);
                response = nil
            end
        end

        handle_response("ListIdentifiers", response)
    end

    def list_records(metadata_prefix: nil, resumption_token: nil)
        raise ArgumentError, 'Need to specify either metadata_prefix or resumption_token' unless metadata_prefix || resumption_token
        raise ArgumentError, 'Must specify one of metadata_prefix or resumption_token' if metadata_prefix && resumption_token

        if metadata_prefix
            arg="metadataPrefix="+metadata_prefix
        else
            arg="resumptionToken="+resumption_token
        end
        # Whether the server has already responded successfully in the past.
        # Using the presence of the resumptionToken to determine this is,
        # perhaps, questionable.
        hasServerResponded = resumption_token

        response = nil
        attempts = 0
        while !response
            begin
                attempts += 1

                starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
                response = self.class.get(@base_uri + "?verb=ListRecords&" + arg)
                ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
                $stats.accept(ending - starting)

                if response.code >= 500
                    # 5xx status code indicates a server error.  Retrying might
                    # help.
                    raise HTTParty::Error.new "Received status code #{response.code}"
                elsif response.code >= 400
                    # 4xx status code indicates a client error.  Retrying with
                    # the same request will not help.
                    raise FailedOaiPmhRequest.new("ListRecords", "Received status code #{response.code}")
                end
            rescue HTTParty::Error, SocketError, Timeout::Error => e
                # FIXME output needs to take into account current whether to
                # insert a new-line.  As a quick hack, use the presence of
                # resumptionToken in the URL to indicate a newline is needed
                # (this works most of the time, but not always)
                if hasServerResponded && attempts == 1
                    print "\n"
                end
                print_with_time("Attempt #{attempts} of #{MAX_HTTP_GET_RETRIES} failed: #{e.message}")

                if attempts >= MAX_HTTP_GET_RETRIES
                    # REVISIT include the URL in the message?
                    raise FailedOaiPmhRequest.new("ListRecords", "Too many GET requests failed")
                end

                delay = FAILED_REQUEST_INITIAL
                if hasServerResponded
                    delay += attempts * FAILED_REQUEST_BACKOFF
                end

                sleep(delay);
                response = nil
            end
        end

        handle_response("ListRecords", response)
    end

    def handle_response(request_type, response)
        if !response.success?
            raise FailedOaiPmhRequest.new(request_type, "HTTP status code #{response.code}")
        end

        if response.body.nil? || response.body.empty?
            raise FailedOaiPmhRequest.new(request_type, "empty entity")
        end

        begin
            xml_response = Nokogiri::XML(response.body)
        rescue StandardError => e
            raise FailedOaiPmhRequest.new(request_type, "XML parsing failed: #{e.message}")
        end
        return xml_response
    end
end

MAX_HTTP_GET_RETRIES = 10

FAILED_REQUEST_INITIAL = 60
FAILED_REQUEST_BACKOFF = 120

class Float
    def signif(signs)
      Float("%.#{signs}g" % self)
    end
end

class SeriesStatistics

    def initialize
        @series = []
        @statistics_valid = false
    end

    def accept(item)
        @series.append(item)
        @statistics_valid = false
    end

    def accept_from(stats)
        @series += stats.instance_variable_get(:@series)
        @statistics_valid = false
    end

    def _calculate_statistics
        largest = nil
        smallest = nil
        @series.each_with_index do | item, index |
            if index == 0
                largest = smallest = item
            else
                if item > largest
                    largest = item
                elsif item < smallest
                    smallest = item
                end
            end
        end
        @max = largest
        @min = smallest

        if !@series.empty?
            sum = @series.reduce(:+)
            @mean = sum / @series.length

            var_sum = @series.inject(0){|accum, i| accum + (i - @mean)**2 }
            @sample_variance = var_sum/(@series.length).to_f
            @standard_deviation = Math.sqrt(@sample_variance)
        else
            @mean = nil
            @sample_variance = nil
            @standard_deviation = nil
        end
    end

    def _conditionally_calculate_statistics
        if !@statistics_valid
            _calculate_statistics
            @statistics_valid = true
        end
    end

    def mean
        _conditionally_calculate_statistics
        @mean
    end

    def standard_deviation
        _conditionally_calculate_statistics
        @standard_deviation
    end

    def count
        @series.length
    end

    def min
        _conditionally_calculate_statistics
        @min
    end

    def max
        _conditionally_calculate_statistics
        @max
    end

    def to_s
        if @series.length == 0
            "N=0"
        elsif @series.length == 1
            "N=1 x=#{(mean).signif(2)}"
        else
            "N=#{count} min=#{(min).signif(2)} μ=#{(mean).signif(2)} max=#{(max).signif(2)} σ=#{(standard_deviation).signif(2)}"
        end
    end
end

# Try to discover someone's email address by looking in various likely
# configuration files.  Returns the email address if it's unambiguous.
def find_email_address
    candidate_emails = []

    # Look in Zoom
    zoomConfig = File.expand_path("~/.config/zoomus.conf")
    if File.file?(zoomConfig)
        File.open( zoomConfig ).each do |line|
            if line =~ /^userEmailAddress=(.*?)$/
                candidate_emails.append($1)
            end
        end
    end

    # TODO Look in Thunderbird
    #find ~.thunderbird -name prefs.js | xargs sed -n 's/user_pref."mail.identity.id1.useremail", "\([^"]*\)".*/\1/p'

    #  Return email address if there is no ambiguity.
    return candidate_emails.uniq.count == 1 ? candidate_emails[0] : nil
end

def check_oai_pmh_endpoint(endpoint_url)
    queryIdentify_url = endpoint_url + "?verb=Identify"
    begin
        xml_response = $client.identify()

        oai_pmh_tag = xml_response.at_xpath('//*[name()="OAI-PMH"]')
        if !oai_pmh_tag
            raise StandardError.new "no OAI-PMH tag."
        end

        addresses = []
        adminEmails = xml_response.xpath('/xmlns:OAI-PMH/xmlns:Identify/xmlns:adminEmail')
        adminEmails.each do |adminEmail|
            address = adminEmail.content
            addresses.append(address)
        end

        return "Active", addresses
    rescue StandardError => e
        print_with_time("Error: #{e.message}")
        return "Error", []
    end
end


def list_sets(endpoint)
    sets_info = {}
    begin
        xml_response = $client.list_sets

        sets = xml_response.xpath('//xmlns:set')
        sets.each do |set|
            set_name = nil
            set_spec = nil
            set.elements().each do |child|
                if child.name == 'setName'
                    set_name = child.content()
                elsif child.name == 'setSpec'
                    set_spec = child.content()
                elsif child.name == 'setDescription'
                    # TODO: parse description to extract DublinCore dc:description
                end
            end

            if set_spec
                sets_info.store(set_spec, set_name ? set_name : set_spec)
            end
        end
    rescue StandardError => e
        print_with_time("Error: ListSets request failed: #{e.message}")
    end
    return sets_info
end


def metadata_formats()
    xml_response = $client.list_metadata_formats

    formats = {}

    metadataFormats = xml_response.xpath("/xmlns:OAI-PMH/xmlns:ListMetadataFormats/xmlns:metadataFormat")
    if !metadataFormats
        raise FailedOaiPmhRequest.new "ListMetadataFormats", "missing metadataFormat elements."
    end

    metadataFormats.each do |metadataFormat|
        prefix = nil
        schema = nil
        namespace = nil

        metadataFormat.children.each do |child|
            next unless child.element?
            if child.name == "metadataPrefix"
                prefix = child.content
            elsif child.name == "schema"
                schema = child.content
            elsif child.name == "metadataNamespace"
                namespace = child.content
            end
        end

        if prefix
            formats[prefix] = {schema: schema, namespace: namespace}
        else
            puts "Found metadataFormat without a prefix"
        end
    end

    return formats
end


def select_datacite(formats)
    formats.each do |(key,value)| # REVISIT, given a choice, which one to pick?
        ns = value[:namespace]
        if DATACITE_NAMESPACES.include? ns
            return key, false
        end

        if DATACITE_WRAPPED_NAMESPACES.include? ns
            return key, true
        end
    end

    return nil, nil
end


def list_identifiers(metadata_prefix: nil, resumption_token: nil)

    xml_response = $client.list_identifiers(metadata_prefix: metadata_prefix, resumption_token: resumption_token)

    total = 0
    set_counts = {}
    resumptionToken = nil

    # NB At 2024-12-20, ILL and PSI endpoints returns malformed responses with
    # the expected "header" elements contained within rogue "record" elements;
    # for example,
    # [...]
    #     <record>
    #       <header>
    #         <identifier>10.5291/ILL-DATA.TEST-3172</identifier>
    #         <datestamp>2021-09-23T12:08:08Z</datestamp>
    #       </header>
    #     </record>
    #   </ListIdentifiers>
    # </OAI-PMH>
    headers = xml_response.xpath("/xmlns:OAI-PMH/xmlns:ListIdentifiers/xmlns:header")
    if headers
        headers.each do |header|
            next if header.attr('status') == 'deleted'
            total += 1
            header.children.each do |child|
                next unless child.element? && child.name == "setSpec"
                setSpec = child.content
                if set_counts.key?(setSpec)
                    set_counts[setSpec] += 1
                else
                    set_counts[setSpec] = 1
                end
            end
        end
    end

    token = xml_response.at_xpath('/xmlns:OAI-PMH/xmlns:ListIdentifiers/xmlns:resumptionToken/text()')
    resumptionToken = token ? token.text : nil

    return total, set_counts, resumptionToken
end

def print_with_time(text)
    print "    #{Time.now.strftime("%d/%m/%Y %H:%M:%S")} #{text}\n"
end


def count_identifiers(endpoint, prefix)
    overall_stats = SeriesStatistics.new
    $stats = SeriesStatistics.new
    overall_total, overall_set_counts, resumptionToken = list_identifiers(metadata_prefix: prefix)

    i = 0
    while resumptionToken
        if i % 40 == 0
            if i > 0
                print("\n")
                print_with_time("#{overall_total} records so far; timing/s #{$stats}")
                overall_stats.accept_from $stats
                $stats = SeriesStatistics.new
            end
            print "    Harvesting Dublin Core identifiers: "
        end
        i += 1
        print "."
        delta_total, delta_set_counts, resumptionToken = list_identifiers(resumption_token: resumptionToken)

        overall_total += delta_total
        delta_set_counts.each do |set_id, delta_count|
            new_count = overall_set_counts.key?(set_id) ? (overall_set_counts[set_id] + delta_count) : delta_count
            overall_set_counts[set_id] = new_count
        end
    end
    if i > 0
        print "\n"
        if overall_stats.count > 0
            print_with_time("#{overall_total} records so far; timing/s #{$stats}")
        end
    end
    overall_stats.accept_from $stats
    print_with_time("Dublin Core identifiers overall timing/s #{overall_stats}")

    return overall_total, overall_set_counts
end

RESOURCE_TYPE_EXPRESSION_WRAPPED = '*[local-name()="metadata"]/*[local-name()="oai_datacite"]/*[local-name()="payload"]/*[local-name()="resource"]/*[local-name()="resourceType"]'
RESOURCE_TYPE_EXPRESSION_UNWRAPPED = '*[local-name()="metadata"]/*[local-name()="resource"]/*[local-name()="resourceType"]'

def list_datacite_records(is_wrapped: false, metadata_prefix: nil, resumption_token: nil)

    xml_response = $client.list_records(metadata_prefix: metadata_prefix, resumption_token: resumption_token)

    total = {}
    set_counts = {}
    resumptionToken = nil

    resource_type_expression = is_wrapped ? RESOURCE_TYPE_EXPRESSION_WRAPPED : RESOURCE_TYPE_EXPRESSION_UNWRAPPED
    records = xml_response.xpath("/o:OAI-PMH/o:ListRecords/o:record",
            {'o' => 'http://www.openarchives.org/OAI/2.0/'})
    if records
        records.each do |record|
            resourceTypeElements = record.xpath(resource_type_expression)
            next if resourceTypeElements.count != 1
            resourceType = resourceTypeElements[0].attr('resourceTypeGeneral')
            next if !resourceType

            total[resourceType] ||= 0
            total[resourceType] += 1

            setSpecElements = record.xpath('o:header/o:setSpec', {'o' => 'http://www.openarchives.org/OAI/2.0/'})
            next if setSpecElements.empty?

            setSpecElements.each do |setSpecElement|
                setSpec = setSpecElement.content
                set_counts[setSpec] ||= {}
                set_counts[setSpec] [resourceType] ||= 0
                set_counts[setSpec] [resourceType] += 1
            end
        end
    end

    token = xml_response.at_xpath('/o:OAI-PMH/o:ListRecords/o:resumptionToken/text()',
            {'o' => 'http://www.openarchives.org/OAI/2.0/'})
    resumptionToken = token ? token.text : nil

    return total, set_counts, resumptionToken
end

def count_datacite_records(prefix, wrapped)
    overall_stats = SeriesStatistics.new
    $stats = SeriesStatistics.new
    overall_total, overall_set_counts, resumptionToken = list_datacite_records(is_wrapped: wrapped, metadata_prefix: prefix)

    i = 0
    while resumptionToken
        if i % 40 == 0
            if i > 0
                print("\n")
                print_with_time("#{overall_total.values.reduce(:+)} records so far; timing/s #{$stats}")
                overall_stats.accept_from $stats
                $stats = SeriesStatistics.new
            end
            print "    Harvesting DataCite records: "
        end
        i += 1
        print "."
        delta_total, delta_set_counts, resumptionToken = list_datacite_records(is_wrapped: wrapped, resumption_token: resumptionToken)

        delta_total.each do |(type,count)|
            overall_total[type] ||= 0
            overall_total[type] += count
        end

        delta_set_counts.each do |(set,set_counts)|
            overall_set_counts[set] ||= {}
            overall_set = overall_set_counts[set]

            set_counts.each do |(type,count)|
                overall_set[type] ||= 0
                overall_set[type] += count
            end
        end
    end

    if i > 0
        print "\n"
        if overall_stats.count > 0
            print_with_time("#{overall_total.values.reduce(:+)} records so far; timing/s #{$stats}")
        end
    end
    overall_stats.accept_from $stats
    print_with_time("DataCite records overall timing/s #{overall_stats}")

    return overall_total, overall_set_counts
end


def query_oai_pmh_endpoint(endpoint, do_harvesting)
    status, adminAddress = check_oai_pmh_endpoint(endpoint)
    if status == "Error"
        return status, [], {}, 0, {}
    end

    set_names = list_sets(endpoint)

    if !do_harvesting
        return "Active", adminAddress, {}, 0, {}
    end

    begin
        total_count, set_counts = count_identifiers(endpoint, "oai_dc")
    rescue StandardError => e
        print_with_time("Error: ListIdentifiers failed: #{e.message}")
        total_count = 0
        set_counts = {}
    end

    formats = metadata_formats()
    prefix, is_wrapped = select_datacite(formats)
    if prefix
        begin
            datacite_totals, set_datacite_counts = count_datacite_records(prefix, is_wrapped)
        rescue StandardError => e
            print_with_time("Error: DataCite harvesting failed: #{e.message}")
            datacite_totals = {}
            set_datacite_counts = {}
        end
    else
        puts "    Skipping DataCite harvesting as DataCite is not supported."
        datacite_totals = {}
        set_datacite_counts = {}
    end

    return status, adminAddress, set_names, total_count, set_counts, datacite_totals, set_datacite_counts
end


up_to_date = true
file_path = '_data/facilities.yml'
facilities = YAML.safe_load(File.read(file_path), permitted_classes: [Date])

email_address = find_email_address
if email_address
    puts "INFO: identifying OAI-PMH activity as from <#{email_address}>"
else
    puts "WARNING: unable to discover your email address.  Carrying on under a cloak of anonymity."
end

facilities.each do |facility|
    odr = facility['odr']
    next unless odr && odr.key?('oai-pmh-endpoint')

    oai_pmh = odr['oai-pmh-endpoint']
    next unless oai_pmh && oai_pmh.key?('link')

    oai_pmh_endpoint = oai_pmh['link']
    next unless oai_pmh_endpoint

    $client = OaiPmhClient.new(oai_pmh_endpoint, email_address)

    expected_status = oai_pmh['status']

    name = facility['short-name']
    puts "Checking OAI-PMH endpoint for #{name}: #{oai_pmh_endpoint}"
    do_harvesting = !oai_pmh['skip-harvesting']
    status, adminAddress, set_names, total_count, set_count, datacite_totals, set_datacite_counts = query_oai_pmh_endpoint(oai_pmh_endpoint, do_harvesting)

    oai_pmh['status'] = status

    if !adminAddress.empty?
        oai_pmh['adminAddress'] = adminAddress
    end

    oai_pmh.delete('items')

    if status == "Active" && do_harvesting
        items = {}
        oai_pmh['items'] = items
        items['count'] = total_count

        if !datacite_totals.empty?
            items['resource_types'] = {}
            datacite_totals.sort_by { |k, _| k }.each do |(key,value)|
                items['resource_types'][key] = value
            end
        end

        if !set_count.empty?
            sets = {}
            items['sets'] = sets

            sets['count'] = set_count.length()

            sets_details = {}
            sets['details'] = sets_details

            set_count.sort_by { |k, _| k }.each do |set_id, count|
                set = {}
                sets_details[set_id] = set

                if set_names.key?(set_id)
                    set['name'] = set_names[set_id]
                end
                set['count'] = count

                if set_datacite_counts.key?(set_id)
                    set['resource_types'] = {}

                    set_datacite_counts[set_id].sort_by { |k, _| k }.each do |(key,value)|
                        set['resource_types'][key] = value
                    end
                end
            end
        end
    end

    oai_pmh['last-check'] = Date.today
end

File.open(file_path, 'w') {|f| f.write facilities.to_yaml }

exit(1) unless up_to_date
