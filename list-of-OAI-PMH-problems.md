# List of OAI-PMH issues

This file contains a list of identified issues with various PaNOSC OAI-PMH
endpoints.  These endpoints are listed at the
[LEAPS WG3 page](https://leaps-wg3.desy.de/open-data-resources.html#gotofacility).

## Common issues

A number of issues are common to multiple facilities, or that require further
investigation.

1. One potential issue is compliance with [the OpenAIRE
guidelines](https://guidelines.openaire.eu/en/latest/), in particular, the
[guidelines related to Data
Archives](https://guidelines.openaire.eu/en/latest/data/index.html).

The [section on
OAI-PMH](https://guidelines.openaire.eu/en/latest/data/use_of_oai_pmh.html) is
annoyingly vague on certain key questions.  [Issue
43](https://github.com/openaire/guidelines/issues/43) identifies the vagueness,
asking for clarification.

Once the requirements are clarified, we will need to evaluate what further
steps are needed for compliance.  This would be easier if there were a standard
tool to do this (see [Issue
45](https://github.com/openaire/guidelines/issues/45))

2. Several facilities use a `resourceTypeGeneral` values of `Collection` (ESRF,
HZB).  This suffers from an ambiguity on how to represent the items within the
collection (see [DataCite discussion
146](https://github.com/datacite/datacite-suggestions/discussions/146)).

Would it be better to described resources as a `Dataset` instead of a
`Collection`?

3. Many facilities support DataCite v3.  DataCite [have
announced](https://datacite.org/blog/deprecating-schema-3/) that they have
deprecated DataCite metadata v3.  This problem has been reported to OpenAIRE as
[issue 42](https://github.com/openaire/guidelines/issues/42).

## ALBA

No problems.

## Elettra

1. The endpoint currently fails with HTTP status code 502.

```console
paul@monkeywrench:~$ curl https://data.ceric-eric.eu/oaipmh/request?verb=Identify
<html>
<head><title>502 Bad Gateway</title></head>
<body bgcolor="white">
<center><h1>502 Bad Gateway</h1></center>
<hr><center>nginx/1.14.0 (Ubuntu)</center>
</body>
</html>
paul@monkeywrench:~$
```

The problem appears some time between 2024-12-22 and 2025-01-01.


## ESRF

1. The endpoint is somewhat slow at responding.  On average, takes some 800 ms
   to respond to ListIdentifiers requests.  For comparison, HZDR takes some 80
   ms to respond to such requests.

2. The GetRecord request produces non-complying DataCite output: there's a
   rogue `I` in the output, immediately after the `dates` metadata property.

```console
paul@monkeywrench:~$ curl -s 'https://icatplus.esrf.fr/oaipmh/request?verb=ListRecords&metadataPrefix=oai_datacite'|grep -B1 -A1 'dates>'
<resourceType resourceTypeGeneral="Collection">Data from large facility measurement</resourceType>
<dates>
<date dateType="Collected">2016-11-22T15:04:35.676Z/2020-02-21T07:00:00Z</date>
<date dateType="Accepted">2020-02-21T07:00:00Z</date>
</dates>I<rightsList>
<rights rightsIdentifier="CC-BY-4.0" rightsURI="https://creativecommons.org/licenses/by/4.0">CC-BY-4.0</rights>
--
[...]
```

3. See above Collection vs Dataset.

## ESS

1. Endpoint responds with a 403 HTTP status code.

```console
paul@monkeywrench:~$ curl -s -D- 'https://oai.panosc.ess.eu/openaire/oai?verb=Identify'
HTTP/1.0 403 Forbidden
cache-control: no-cache
content-type: text/html

<html><body><h1>403 Forbidden</h1>
Request forbidden by administrative rules.
</body></html>

paul@monkeywrench:~$
```

## European XFEL

1. Malformed response:

```console
paul@monkeywrench:~$ curl -s https://in.xfel.eu/metadata/oai-pmh/oai2?verb=Identify|xmllint -format -|grep request
  <request>https://in.xfel.eu/metadata{"verb"=&gt;"Identify"}</request>
paul@monkeywrench:~$
```

2. Missing `ResourceType` metadata property in DataCite metadata.

```console
paul@monkeywrench:~$ curl -s 'https://in.xfel.eu/metadata/oai-pmh/oai2?verb=ListRecords&metadataPrefix=oai_datacite'|xmllint -format -|grep resourceType
paul@monkeywrench:~$
```

3. The `description` elements (in `Descriptions` metadata property) use default
XML namespace.

```console
paul@monkeywrench:~$ curl -s 'https://in.xfel.eu/metadata/oai-pmh/oai2?verb=ListRecords&metadataPrefix=oai_datacite'|xmllint -format -|grep description|head -2|cut -c-80
          <datacite:descriptions>
            <description xmlns="" descriptionType="Abstract">The European XFEL (
paul@monkeywrench:~$
```

4. The `creator` elements (in `Creators` metadata property) use the default XML
namespace.

```console
paul@monkeywrench:~$ curl -s 'https://in.xfel.eu/metadata/oai-pmh/oai2?verb=ListRecords&metadataPrefix=oai_datacite'|xmllint -format -|grep creator|head -5
          <datacite:creators>
            <creator xmlns="">
              <creatorName>Fabio Dall'Antonia</creatorName>
            </creator>
          </datacite:creators>
paul@monkeywrench:~$
```

## HZB

1. See above common issue on Collection vs Dataset.

## HZDR

1. Some of the set names have rogue spaces at beginning or end.

```console
paul@monkeywrench:~$ curl -s https://rodare.hzdr.de/oai2d?verb=ListSets | grep 'setName> '
      <setName> ELBE (Electron Linac for beams with high Brilliance and low Emittance)</setName>
paul@monkeywrench:~$ curl -s https://rodare.hzdr.de/oai2d?verb=ListSets | grep ' </setName>'
      <setName>Bremsstrahlung (γELBE) </setName>
      <setName>ZRT - Institute of Radiopharmaceutical Cancer Research </setName>
paul@monkeywrench:~$
```

2. Perhaps acronym don't need to be expanded in `setName`, but doing this in
`setDescription` would be sufficient?


## ILL

1. The `ListIdentifiers` output is invalid as it mistakenly includes the
`record` element.

```console
paul@monkeywrench:~$ curl -s 'https://fairdata.ill.fr/openaire/oai?verb=ListIdentifiers&metadataPrefix=oai_dc' | xmllint -format - | head -10
<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/  http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate>2024-09-13T07:35:31.803Z</responseDate>
  <request verb="ListIdentifiers" metadataPrefix="oai_dc">localhost</request>
  <ListIdentifiers>
    <record>
      <header>
        <identifier>10.5291/ILL-DATA.CZ-1</identifier>
        <datestamp>2020-12-23T15:05:22Z</datestamp>
      </header>
paul@monkeywrench:~$
```

This can also be seen because the output doesn't validate:

```console
paul@monkeywrench:~$ curl -s 'https://fairdata.ill.fr/openaire/oai?verb=ListIdentifiers&metadataPrefix=oai_dc'|xmllint --format -|sed -n 's/.*xsi:schemaLocation=\"\([^"]*\)".*/\1/p'
http://www.openarchives.org/OAI/2.0/  http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd
paul@monkeywrench:~$ curl -s -L -O http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd
paul@monkeywrench:~$ curl -s 'https://fairdata.ill.fr/openaire/oai?verb=ListIdentifiers&metadataPrefix=oai_dc'|xmllint --schema OAI-PMH.xsd --noout -
-:2: element record: Schemas validity error : Element '{http://www.openarchives.org/OAI/2.0/}record': This element is not expected. Expected is ( {http://www.openarchives.org/OAI/2.0/}header ).
- fails to validate
paul@monkeywrench:~$
```

## ISIS

1. Endpoint is very slow (8--10 seconds per request).  I think we should target
   a sub-one-second response time.

2. After about 9 hours of harvesting, the response from a ListIdentifiers
   request includes a `resumptionToken` that, when included in a subsequent
   request, results in the server rejecting that request.

## MAX IV

1. Endpoint is broken.  It returns a SciCat page, rather than an OAI-PMH
   response:

```console
paul@monkeywrench:~$ curl -s https://scicat.maxiv.lu.se/openaire/oai?verb=Identify | grep OAI-PMH
paul@monkeywrench:~$
```

## PSI

1. The `ListIdentifiers` output is invalid as it mistakenly includes the
   `record` element.

```console
paul@monkeywrench:~$ curl -s 'https://doi.psi.ch/oaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_dc' | xmllint -format - | head -10<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/  http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate>2024-07-20T20:29:54Z</responseDate>
  <request verb="ListIdentifiers" metadataPrefix="oai_dc">https://doi.psi.ch/oaipmh/oai</request>
  <ListIdentifiers>
    <record>
      <header>
        <identifier>10.16907/808de0df-a9d3-4698-8e9f-d6e091516650</identifier>
        <datestamp>2020-05-19T14:16:43Z</datestamp>
      </header>
paul@monkeywrench:~$
```

This can also be seen because the output doesn't validate:

```console
paul@monkeywrench:~$ curl -s 'https://doi.psi.ch/oaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_dc'|xmllint --format -|sed -n 's/.*xsi:schemaLocation=\"\([^"]*\)".*/\1/p'
http://www.openarchives.org/OAI/2.0/  http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd
paul@monkeywrench:~$ curl -s -L -O http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd
paul@monkeywrench:~$ curl -s 'https://doi.psi.ch/oaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_dc' | xmllint --schema OAI-PMH.xsd --noout -
-:2: element record: Schemas validity error : Element '{http://www.openarchives.org/OAI/2.0/}record': This element is not expected. Expected is ( {http://www.openarchives.org/OAI/2.0/}header ).
- fails to validate
paul@monkeywrench:~$
```

2. The server claims to support DataCite

```console
paul@monkeywrench:~$ curl -s https://doi.psi.ch/oaipmh/oai?verb=ListMetadataFormats | xmllint --format - | grep -B1 -A3 oai_datacite
    <metadataFormat>
      <metadataPrefix>oai_datacite</metadataPrefix>
      <schema>http://schema.datacite.org/meta/kernel-3/metadata.xsd</schema>
      <metadataNamespace>http://datacite.org/schema/kernel-3</metadataNamespace>
    </metadataFormat>
paul@monkeywrench:~$
```

However, when queries for DataCite records, the server responds with Dublin
Core metadata:

```console
paul@monkeywrench:~$ curl -s 'https://doi.psi.ch/oaipmh/oai?verb=ListRecords&metadataPrefix=oai_datacite' | xmllint --format - | head -15 | cut -c-80
<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.o
  <responseDate>2024-07-20T20:29:54Z</responseDate>
  <request verb="ListRecords" metadataPrefix="oai_datacite">https://doi.psi.ch/o
  <ListRecords>
    <record>
      <header>
        <identifier>10.16907/808de0df-a9d3-4698-8e9f-d6e091516650</identifier>
        <datestamp>2020-05-19T14:16:43Z</datestamp>
      </header>
      <metadata>
        <oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xm
          <dc:title>JUNGFRAU detector for brighter X-ray sources - solutions for
          <dc:description>Lysozyme crystal measured at 100 deg/s with JUNGFRAU 4
</dc:description>
paul@monkeywrench:~$
```

## SOLEIL

1. The client connects (TCP and TLS) successfully to the server:

```console
paul@monkeywrench:~$ openssl s_client -brief -connect datacatalog.synchrotron-soleil.fr:443 </dev/null
CONNECTION ESTABLISHED
Protocol version: TLSv1.3
Ciphersuite: TLS_AES_256_GCM_SHA384
Peer certificate: C = FR, ST = \C3\8Ele-de-France, O = Synchrotron Soleil, CN = *.synchrotron-soleil.fr
Hash used: SHA256
Signature type: RSA-PSS
Verification: OK
Server Temp Key: ECDH, prime256v1, 256 bits
DONE
paul@monkeywrench:~$ echo $?
0
paul@monkeywrench:~$
```

However, any HTTP requests make over this connection result in the server
closing the request:

```console
paul@monkeywrench:~$ curl https://datacatalog.synchrotron-soleil.fr/scicat/oai?verb=Identify
curl: (92) HTTP/2 stream 1 was not closed cleanly: STREAM_CLOSED (err 5)
paul@monkeywrench:~$ curl --http1.1 https://datacatalog.synchrotron-soleil.fr/scicat/oai?verb=Identify
curl: (52) Empty reply from server
paul@monkeywrench:~$
```
